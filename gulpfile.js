/**
 * Created by dennismuys on 01/12/2016.
 */
'use strict';
var gulp = require('gulp');

var webserver = require('gulp-webserver');

gulp.task('webserver', function() {
    gulp.src('./')
        .pipe(webserver({
            livereload: true,
            directoryListing: {
                enable:false,
                path: './'
            },
            open: true
        }));
});

var sass = require('gulp-sass');
gulp.task('sass', function () {
    gulp.src('./assets/sass/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./assets/css'));
});

gulp.task('sass:watch',['webserver'], function () {
    gulp.watch('./assets/sass/**/*.scss', ['sass']);
});
