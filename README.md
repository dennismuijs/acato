# README #

Bij deze de assignment zoals ontvangen van acato.
Ik heb hier in totaal iets minder dan 6 uur aan gewerkt.

## Hoe werkt dit eigenlijk? ##

* Download de master branch
* Zorg dat **npm** en **gulp** op het systeem geïnstalleerd staan
* Run **npm install** vanuit de projectfolder
* Run **gulp sass:watch**
* Een server word opgestart op http://localhost:8000
* Start de pagina in een browser naar voorkeur zolang je voorkeur niet internet explorer is

## Het hoe en waarom ##

Ik heb gekozen voor een soort galerij met foto's die op de achterkant wat informatie bevatten.
De foto's heb ik van de site van acato af kunnen halen door de div id:smoelen te kopiëren en hem snel als JSON array te formatteren.
Via een ng-repeat kon ik alle foto's (plus 1) makkelijk opmaken en tonen.
het contactformulier is een onderdeel wat hergebruikt zou kunnen worden in andere angular applicaties dus heb ik daar een aparte module van gemaakt.
Met behulp van angular form validation zorg ik dat de juiste velden ingevuld zijn voor de submit knop ingedrukt kan worden.

## Wat zou ik veranderen? ##

* Testen testen en testen.
  Ik heb deze opdracht op een mac gemaakt met alleen beschikking tot safari en chrome.
* Ik weet dus niet wat er gebeurt als de pagina op IE bekeken word, ik verwacht een plotseling einde van het universum dus pas op.
* Ik zou ook verder kijken waarom safari plotseling moeite heeft met -webkit-border-image. nu heb ik dat opgelost door de entry zonder prefix te verwijderen.
* Ik zou ook graag de contactmodule wat verder uit willen breiden met bijvoorbeeld validatie voor telefoonnummers, ik had hier een mooie regex voor maar ik kreeg hem niet meer in het project voor mijn gestelde deadline.
* Ik zou ook graag meer gebruik maken van pre-processors, dit was mijn eerste project waarin ik ze gebruik en ik zie veel voordelen.
De reden dat dit nooit kon op mijn vorige baan was dat we veel met legacy html en css moesten werken en pre-processors deden meer kwaad dan goed.