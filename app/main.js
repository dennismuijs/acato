/**
 * Created by dennismuys on 01/12/2016.
 */

"use strict";

var acato = angular.module("acato", ['ngAnimate', 'ui.bootstrap', 'contact']);

acato.config(['$compileProvider',function($compileProvider){
    $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|http?|file|maps|geo|tel|mailto|sms|chrome-extension):/);
}]);

acato.controller('MainCtrl',['$scope','employeeService', function($scope,employeeService){
    employeeService.getEmployees().then(function(response){
        $scope.employees = response;
    });
}]);