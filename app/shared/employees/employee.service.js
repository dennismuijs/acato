/**
 * Created by dennismuys on 01/12/2016.
 */
"use strict";

acato.service('employeeService',['$http','$q',function($http, $q){
    var service = {
        getEmployees: function () {
            return $q(function(resolve) {
                $http.get('../assets/API_static/medewerkers.json')
                    .then(function (res) {
                        res.data.push({
                            id:"99",
                            image:"../assets/img/user.JPG",
                            title:"acato, Dennis Muijs, ???"
                        });
                        resolve(res.data);
                    });
            });
        }
    };
    return service;
}]);